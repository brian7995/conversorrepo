//
//  SeleccionarMonedaVC.swift
//  ConversorMonedas
//
//  Created by Brian Nuñez on 8/15/20.
//  Copyright © 2020 MasUno. All rights reserved.
//

import UIKit

class SeleccionarMonedaVC: UIViewController,UITableViewDataSource,UITableViewDelegate {
 
    @IBOutlet weak var monedasTV: UITableView!
    
    var paises = [Pais]()
    
    var paisSeleccionado : Pais?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        monedasTV.dataSource = self
        monedasTV.delegate = self
        
        paises = Pais.obtenerPaises()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return paises.count
    }
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "monedaCell")!
        
        let banderaImg = cell.viewWithTag(100) as! UIImageView
        let nombre = cell.viewWithTag(101) as! UILabel
        let moneda = cell.viewWithTag(102) as! UILabel
        
        let pais = paises[indexPath.row]
        
        banderaImg.image = UIImage(named: pais.nombre)
        nombre.text = pais.nombre
        moneda.text = pais.moneda + ", " + pais.codigo
        
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        self.paisSeleccionado = self.paises[indexPath.row]
        self.performSegue(withIdentifier: "volverConversor", sender: nil)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
