//
//  SplashVC.swift
//  ConversorMonedas
//
//  Created by Brian Nuñez on 8/15/20.
//  Copyright © 2020 MasUno. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    
    @IBOutlet weak var imgView: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        imgView.image = imgView.image?.withRenderingMode(.alwaysTemplate)
        
        if self.traitCollection.userInterfaceStyle == .dark {
            print("Dark")
            imgView.tintColor = UIColor.white
        } else {
            print("Light")
            imgView.tintColor = UIColor.black
        }
        
        Timer.scheduledTimer(withTimeInterval: 3, repeats: false) { (timer) in
            self.performSegue(withIdentifier: "goConversor", sender: nil)
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
