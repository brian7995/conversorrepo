//
//  ViewController.swift
//  ConversorMonedas
//
//  Created by Brian Nuñez on 8/15/20.
//  Copyright © 2020 MasUno. All rights reserved.
//

import UIKit

class ConversorVC : UIViewController {

    @IBOutlet weak var viewContenedor: UIView!
    @IBOutlet weak var enviarLb: UILabel!
    @IBOutlet weak var montoTxt: UITextField!
    @IBOutlet weak var recibirLb: UILabel!
    @IBOutlet weak var valorReciboLb: UILabel!
    @IBOutlet weak var btnMoneda1: UIButton!
    @IBOutlet weak var btnMoneda2: UIButton!
    @IBOutlet weak var btnIntercambiar: UIButton!
    @IBOutlet weak var compraVentaLb: UILabel!
    
    var empujar = true
    var centerYViewContenedor : NSLayoutConstraint!
    var tagBtn = 1
    
    var paisUno : Pais!
    var paisDos : Pais!
    
    var compra : Double = 0.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.configurarVistas()
        self.activarOcultamientoTeclado()
    }
    
    override func viewWillAppear(_ animated: Bool)
    {
        super.viewWillAppear(animated)
        self.empujar = true
    }
    
    //MARK: CONFIGURAR LAS VISTAS
    func configurarVistas()
    {
        self.view.layoutIfNeeded()
        
        viewContenedor.clipsToBounds = true
        viewContenedor.layer.cornerRadius = 5
        viewContenedor.layer.borderColor = UIColor.gray.cgColor
        viewContenedor.layer.borderWidth = 1
        
        centerYViewContenedor = self.view.constraints.first(where: {return $0.identifier == "centerY"})!
        
        btnIntercambiar.clipsToBounds = true
        btnIntercambiar.layer.cornerRadius = btnIntercambiar.bounds.height / 2
        
        //Cuando el iphone es pequeño , cambio el tamaño de varias vistas.
        switch(UIScreen.main.bounds.size.height)
        {
        case 480:
            print("iphone 4")
            self.cambiarTamaños(lbls: 13, valores: 17, monedas: 14)
        case 568:
            print("iphone 5")
            self.cambiarTamaños(lbls: 14, valores: 18, monedas: 15)
        default:
            break
        }
        
        let longTap1 = UILongPressGestureRecognizer(target: self, action: #selector(irSeleccionarMoneda(gesture:)))
        let longTap2 = UILongPressGestureRecognizer(target: self, action: #selector(irSeleccionarMoneda(gesture:)))
        
        btnMoneda1.addGestureRecognizer(longTap1)
        btnMoneda2.addGestureRecognizer(longTap2)
        
        montoTxt.delegate = self
        montoTxt.addDoneCancelToolbar()
        
        //Estos son los paises de las monedas por defecto
        
        paisUno = Pais.obtenerPaises().first(where: {return $0.nombre == "Unión Europea"})!
        paisDos = Pais.obtenerPaises().first(where: {return $0.nombre == "Estados Unidos"})!
        
        self.pintarValores()
    }
    
    func cambiarTamaños(lbls:CGFloat,valores:CGFloat,monedas:CGFloat)
    {
        enviarLb.font = enviarLb.font.withSize(lbls)
        recibirLb.font = recibirLb.font.withSize(lbls)
        
        montoTxt.font = montoTxt.font?.withSize(valores)
        valorReciboLb.font = valorReciboLb.font.withSize(valores)
        
        btnMoneda1.titleLabel?.font = btnMoneda1.titleLabel?.font.withSize(monedas)
        btnMoneda2.titleLabel?.font = btnMoneda1.titleLabel?.font.withSize(monedas)
    }
    
    func pintarValores()
    {
        btnMoneda1.setTitle(paisUno.moneda, for: .normal)
        btnMoneda2.setTitle(paisDos.moneda, for: .normal)
        
        let codigos = paisUno.codigo + "-" + paisDos.codigo
        
        switch paisUno.codigo {
        case "EUR":
            compra = TipoCambio.tipoCambioEuro(codigos: codigos)
        case "USD":
            compra = TipoCambio.tipoCambioDolar(codigos: codigos)
        case "JPY":
            compra = TipoCambio.tipoCambioYen(codigos: codigos)
        case "GBP":
            compra = TipoCambio.tipoCambioLibra(codigos: codigos)
        case "CHF":
            compra = TipoCambio.tipoCambioFrancoSuizo(codigos: codigos)
        case "CAD":
            compra = TipoCambio.tipoCambioDolarCanadiense(codigos: codigos)
        case "PEN":
            compra = TipoCambio.tipoCambioSol(codigos: codigos)
        default:
            break
        }
              
        //Este es un valor de venta ficticio
        let venta = compra + 0.03
        compraVentaLb.text = "Compra : \(compra) | venta : \(venta.valorNumericoDosDecimales())"
    }
    
    //MARK: ACTIVAR OCULTAMIENTO TECLADO
    func activarOcultamientoTeclado()
    {
        let tapKeyboard = UITapGestureRecognizer(target: self, action: #selector(ocultarTeclado(sender:)))
        self.view.addGestureRecognizer(tapKeyboard)
    }
     
    //MARK: OCULTAR TECLADO
    @objc func ocultarTeclado(sender:UITapGestureRecognizer)
    {
        self.view.endEditing(true)
    }
    
    //MARK: INTERCAMBIAR MONEDAS
    @IBAction func intercambiarMonedas(_ sender: Any)
    {
        print("intercambiamos las monedas")
        (paisUno,paisDos) = (paisDos,paisUno)
        
        self.pintarValores()
        self.convertir()
    }
    
    //MARK: EMPEZAR OPERACION
    @IBAction func empezarOperacion(_ sender: Any)
    {
        self.convertir()
    }

    func convertir()
    {
        guard let monto = Double(montoTxt.text!) else {
            self.mostrarAlerta()
            return
        }
        
        print("Monto :" , monto)
        let valorMonto = monto * compra
        valorReciboLb.text = valorMonto.valorNumericoDosDecimales()
    }
    
    //MARK: MOSTRAR ALERTA DE INGRESAR UN MONTO
    func mostrarAlerta()
    {
        let alerta = UIAlertController(title: "Conversor Monedas", message: "Ingresa un monto porfavor.", preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alerta.addAction(okAction)
        
        self.present(alerta, animated: true, completion: nil)
    }
    
    //MARK: MONEDA A CAMBIAR
    @IBAction func monedaACambiar(_ btn: UIButton)
    {
        self.tagBtn = btn.tag
    }
    
    //MARK: IR SELECCIONAR MONEDA
    @objc func irSeleccionarMoneda(gesture:UILongPressGestureRecognizer)
    {
        if(empujar)
        {
            print("ir seleccionar moneda")
            self.performSegue(withIdentifier: "goMonedas", sender: nil)
        }

        empujar = false
    }
    
    //MARK: MONEDA SELECCIONADA
    @IBAction func monedaSeleccionada(_ segue: UIStoryboardSegue)
    {
        let monedaVC = segue.source as! SeleccionarMonedaVC
        
        if let pais = monedaVC.paisSeleccionado
        {
            print(pais.nombre)
            
            if(tagBtn == 1)
            {
                self.paisUno = pais
            }else
            {
                self.paisDos = pais
            }
            
            self.pintarValores()
            self.convertir()
        }
    }
}

extension ConversorVC : UITextFieldDelegate
{
    func textFieldDidBeginEditing(_ textField: UITextField) {
        print("Abrimos el teclado")
        
        UIView.animate(withDuration: 0.25) {
            self.centerYViewContenedor.constant = -100
            self.view.layoutIfNeeded()
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        print("Cerramos  el teclado")
        
        UIView.animate(withDuration: 0.25) {
            self.centerYViewContenedor.constant = 0
            self.view.layoutIfNeeded()
        }
    }
}
