//
//  TipoCambio.swift
//  ConversorMonedas
//
//  Created by Brian Nuñez on 8/16/20.
//  Copyright © 2020 MasUno. All rights reserved.
//

import UIKit

class TipoCambio: NSObject
{
    //MARK: EUROS
    static func tipoCambioEuro(codigos:String) -> Double
    {
        var dict : [String:Double] = [:]
        
        dict["EUR-EUR"] = 1.0
        dict["EUR-USD"] = 1.185
        dict["EUR-GBP"] = 0.905
        dict["EUR-PEN"] = 4.230
        dict["EUR-CHF"] = 1.077
        dict["EUR-CAD"] = 1.572
        dict["EUR-JPY"] = 126.277
        
        return dict[codigos]!
    }
    
    //MARK: DÓLARES
    static func tipoCambioDolar(codigos:String) -> Double
    {
        var dict : [String:Double] = [:]
        
        dict["USD-USD"] = 1
        dict["USD-EUR"] = 0.844
        dict["USD-GBP"] = 0.764
        dict["USD-PEN"] = 3.570
        dict["USD-CHF"] = 0.909
        dict["USD-CAD"] = 1.327
        dict["USD-JPY"] = 106.590
        
        return dict[codigos]!
    }
    
    //MARK: SOLES
    static func tipoCambioSol(codigos:String) -> Double
    {
        var dict : [String:Double] = [:]
        
        dict["PEN-PEN"] = 1
        dict["PEN-EUR"] = 0.236
        dict["PEN-GBP"] = 0.214
        dict["PEN-USD"] = 0.280
        dict["PEN-CHF"] = 0.255
        dict["PEN-CAD"] = 0.372
        dict["PEN-JPY"] = 29.855
        
        return dict[codigos]!
    }
    
    //MARK: LIBRA
    static func tipoCambioLibra(codigos:String) -> Double
    {
        var dict : [String:Double] = [:]
        
        dict["GBP-GBP"] = 1
        dict["GBP-EUR"] = 1.105
        dict["GBP-PEN"] = 4.672
        dict["GBP-USD"] = 1.309
        dict["GBP-CHF"] = 1.189
        dict["GBP-CAD"] = 1.736
        dict["GBP-JPY"] = 139.477
    
        return dict[codigos]!
    }
    
    //MARK: YEN
    static func tipoCambioYen(codigos:String) -> Double
    {
        var dict : [String:Double] = [:]
        
        dict["JPY-JPY"] = 1
        dict["JPY-EUR"] = 0.008
        dict["JPY-PEN"] = 0.033
        dict["JPY-USD"] = 0.009
        dict["JPY-CHF"] = 0.009
        dict["JPY-CAD"] = 0.012
        dict["JPY-GBP"] = 0.007
        
        return dict[codigos]!
    }
    
    //MARK: DOLAR CANADIENSE
    static func tipoCambioDolarCanadiense(codigos:String) -> Double
    {
        var dict : [String:Double] = [:]
        
        dict["CAD-CAD"] = 1
        dict["CAD-EUR"] = 0.636
        dict["CAD-PEN"] = 2.691
        dict["CAD-USD"] = 0.754
        dict["CAD-CHF"] = 0.685
        dict["CAD-JPY"] = 80.348
        dict["CAD-GBP"] = 0.576
        
        return dict[codigos]!
    }
    
    //MARK: FRANCO SUIZO
    static func tipoCambioFrancoSuizo(codigos:String) -> Double
    {
        var dict : [String:Double] = [:]
        
        dict["CHF-CHF"] = 1
        dict["CHF-EUR"] = 0.929
        dict["CHF-PEN"] = 3.928
        dict["CHF-USD"] = 1.100
        dict["CHF-CAD"] = 0.685
        dict["CHF-JPY"] = 117.287
        dict["CHF-GBP"] = 0.841
    
        return dict[codigos]!
    }

}
