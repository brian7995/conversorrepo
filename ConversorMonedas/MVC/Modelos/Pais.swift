//
//  Pais.swift
//  ConversorMonedas
//
//  Created by Brian Nuñez on 8/15/20.
//  Copyright © 2020 MasUno. All rights reserved.
//

import UIKit

class Pais: NSObject
{
    var nombre : String = ""
    var moneda : String = ""
    var codigo : String = ""
    
    init(nombre:String,moneda:String,codigo:String)
    {
        self.nombre = nombre
        self.moneda = moneda
        self.codigo = codigo
    }
    
    static func obtenerPaises() -> [Pais]
    {
        var paises = [Pais]()
        
        var pais = Pais(nombre: "Unión Europea",moneda: "Euros" ,codigo: "EUR")
        paises.append(pais)
        
        pais = Pais(nombre: "Estados Unidos", moneda : "Dólares" ,codigo: "USD")
        paises.append(pais)
        
        pais = Pais(nombre: "Japón", moneda : "Yenes" ,codigo: "JPY")
        paises.append(pais)
        
        pais = Pais(nombre: "Reino Unido", moneda : "Libras" ,codigo: "GBP")
             paises.append(pais)
        
        pais = Pais(nombre: "Suiza", moneda : "Franco Suizo" ,codigo: "CHF")
        paises.append(pais)
        
        pais = Pais(nombre: "Canadá", moneda : "Dólares Canadienses" ,codigo: "CAD")
        paises.append(pais)
        
        pais = Pais(nombre: "Perú", moneda : "Soles" ,codigo: "PEN")
        paises.append(pais)
        
        return paises
    }

}
