//
//  Extensiones.swift
//  ConversorMonedas
//
//  Created by Brian Nuñez on 8/15/20.
//  Copyright © 2020 MasUno. All rights reserved.
//

import UIKit

extension UITextField {
    func addDoneCancelToolbar(onDone: (target: Any, action: Selector)? = nil, onCancel: (target: Any, action: Selector)? = nil) {
        let onCancel = onCancel ?? (target: self, action: #selector(cancelButtonTapped))
        let onDone = onDone ?? (target: self, action: #selector(doneButtonTapped))
        
        let cancelar = UIBarButtonItem(title: "Cancelar", style: .done, target: onCancel.target, action: onCancel.action)
        
        cancelar.tintColor = UIColor.darkGray
        
        let aceptar = UIBarButtonItem(title: "Aceptar", style: .done, target: onDone.target, action: onDone.action)
        
        aceptar.tintColor = UIColor.darkGray
        
        let toolbar: UIToolbar = UIToolbar()
        toolbar.barStyle = .default
        toolbar.items = [
            cancelar,
            UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil),
            aceptar
        ]
        toolbar.sizeToFit()
        
        self.inputAccessoryView = toolbar
    }
    
    // Default actions:
    @objc func doneButtonTapped() {
        self.restorationIdentifier = "Aceptado"
        self.resignFirstResponder()
    }
    @objc func cancelButtonTapped() {
        self.restorationIdentifier = "Cancelado"
        self.resignFirstResponder()
    }
}

extension Double
{
    func valorNumericoDosDecimales() -> String
    {
        let saldoFormato = NumberFormatter()
        saldoFormato.maximumFractionDigits = 2
        saldoFormato.minimumFractionDigits = 2
        
        let nsNumberPrecio = self as NSNumber
        let valorStr = saldoFormato.string(from: nsNumberPrecio as NSNumber)!
        return valorStr
    }
}
